<?xml version="1.0" encoding="utf-8"?>
<!--
/*
** Copyright 2011, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/
-->

<resources xmlns:xliff="urn:oasis:names:tc:xliff:document:1.2">

    <!-- This device is not "voice capable"; it's data-only. -->
    <bool name="config_voice_capable">false</bool>

    <!-- This device does not allow sms service. -->
    <bool name="config_sms_capable">false</bool>

    <!-- This string array should be overridden by the device to present a list of network
         attributes.  This is used by the connectivity manager to decide which networks can coexist
         based on the hardware -->
    <!-- An Array of "[Connection name],[ConnectivityManager connection type],
         [associated radio-type],[priority],[restoral-timer(ms)],[dependencyMet]  -->
    <!-- the 5th element "resore-time" indicates the number of milliseconds to delay
         before automatically restore the default connection.  Set -1 if the connection
         does not require auto-restore. -->
    <!-- the 6th element indicates boot-time dependency-met value. -->
    <string-array translatable="false" name="networkAttributes">
        <item>"wifi,1,1,2,-1,true"</item>
        <item>"mobile,0,0,0,-1,true"</item>
        <item>"mobile_mms,2,0,2,60000,false"</item>
        <item>"mobile_supl,3,0,2,60000,true"</item>
        <item>"mobile_dun,4,0,2,60000,true"</item>
        <item>"mobile_hipri,5,0,3,60000,true"</item>
        <item>"mobile_fota,10,0,2,60000,true"</item>
        <item>"mobile_ims,11,0,2,60000,true"</item>
        <item>"mobile_cbs,12,0,2,60000,true"</item>
        <item>"wifi_p2p,13,1,0,-1,true"</item>
        <item>"bluetooth,7,7,0,-1,true"</item>
        <item>"ethernet,9,9,9,-1,true"</item>
    </string-array>

    <!-- This string array should be overridden by the device to present a list of radio
         attributes.  This is used by the connectivity manager to decide which networks can coexist
         based on the hardware -->
    <!-- An Array of "[ConnectivityManager connectionType],
                      [# simultaneous connection types]"  -->
    <string-array translatable="false" name="radioAttributes">
        <item>"1,1"</item>
    </string-array>

    <!-- List of regexpressions describing the interface (if any) that represent tetherable
         Wifi interfaces.  If the device doesn't want to support tethering over Wifi this
         should be empty.  An example would be "softap.*" -->
    <string-array translatable="false" name="config_tether_wifi_regexs">
        <item>"wlan0"</item>
        <item>"softap.*"</item>
    </string-array>

    <!-- List of regexpressions describing the interface (if any) that represent tetherable
         USB interfaces.  If the device doesn't want to support tethering over USB this should
         be empty.  An example would be "usb.*" -->
    <string-array translatable="false" name="config_tether_usb_regexs">
        <item>"usb\\d"</item>
        <item>"rndis\\d"</item>
    </string-array>

    <!-- List of regexpressions describing the interface (if any) that represent tetherable
         bluetooth interfaces.  If the device doesn't want to support tethering over bluetooth this
         should be empty. -->
    <string-array translatable="false" name="config_tether_bluetooth_regexs">
        <item>"bt-pan"</item>
    </string-array>

    <!-- Array of allowable ConnectivityManager network types for tethering -->
    <!-- Common options are [1, 4] for TYPE_WIFI and TYPE_MOBILE_DUN or
         [0,1,5,7] for TYPE_MOBILE, TYPE_WIFI, TYPE_MOBILE_HIPRI and TYPE_BLUETOOTH -->
    <integer-array translatable="false" name="config_tether_upstream_types">
        <item>0</item>
        <item>1</item>
        <item>5</item>
        <item>7</item>
        <item>9</item>
    </integer-array>

    <!-- Boolean indicating whether the wifi chipset has dual frequency band support -->
    <bool translatable="false" name="config_wifi_dual_band_support">true</bool>

    <!-- Boolean indicating whether the wifi chipset has background scan support -->
    <bool translatable="false" name="config_wifi_background_scan_support">true</bool>

    <!-- Integer indicating the framework scan interval in milliseconds. -->
    <integer translatable="false" name="config_wifi_framework_scan_interval">10000</integer>

    <!-- Configure wifi tcp buffersizes in the form:
         rmem_min,rmem_def,rmem_max,wmem_min,wmem_def,wmem_max -->
    <string name="config_wifi_tcp_buffers" translatable="false">524288,2097152,4194304,262144,524288,1048576</string>

    <!-- Boolean indicating if current platform supports BLE peripheral mode -->
    <bool name="config_bluetooth_le_peripheral_mode_supported">true</bool>

    <!-- Minimum screen brightness allowed by the power manager. -->
    <integer name="config_screenBrightnessDim">3</integer>

    <!-- If this is true, the screen will come on when you unplug usb/power/whatever. -->
    <bool name="config_unplugTurnsOnScreen">true</bool>

    <!-- Flag indicating whether the we should enable the automatic brightness in Settings.
         Software implementation will be used if config_hardware_auto_brightness_available is not set -->
    <bool name="config_automatic_brightness_available">true</bool>

    <!-- Remote server that can provide NTP responses. -->
    <string translatable="false" name="config_ntpServer">asia.pool.ntp.org</string>

    <!-- Device configuration indicating whether we should avoid using accelerated graphics
         in certain places to reduce RAM footprint.  This is ignored if ro.config.low_ram
         is true (in that case this is assumed true as well).  It can allow you to tune down
         your device's memory use without going to the point of causing applications to turn
         off features. -->
    <bool name="config_avoidGfxAccel">false</bool>

    <!-- Make things go fast -->
    <bool name="config_ui_enableFadingMarquee">false</bool>

    <!--  Whether Multiuser UI should be shown -->
    <bool name="config_enableMultiUserUI">true</bool>

    <!-- Maximum number of supported users -->
    <integer name="config_multiuserMaximumUsers">4</integer>

    <!-- Setting this true forces the headset jack switches to use the/dev/input/event subsystem
         rather than the uevent framework. -->
    <bool name="config_useDevInputEventForAudioJack">true</bool>

    <!-- Whether safe headphone volume is enabled or not (country specific). -->
    <bool name="config_safe_media_volume_enabled">true</bool>

    <!-- Default list of files pinned by the Pinner Service -->
    <string-array translatable="false" name="config_defaultPinnerServiceFiles">
        <item>"/system/framework/arm/boot-framework.oat"</item>
        <item>"/system/framework/oat/arm/services.odex"</item>
        <item>"/system/framework/arm/boot.oat"</item>
        <item>"/system/framework/arm/boot-core-libart.oat"</item>
    </string-array>

    <!-- Separate software navigation bar required on this device. -->
    <bool name="config_showNavigationBar">true</bool>

    <!-- Enable system navigation keys. -->
    <bool name="config_supportSystemNavigationKeys">true</bool>

    <!-- Translucent status & navigation bars perform poorly on this device -->
    <bool name="config_enableTranslucentDecor">false</bool>

</resources>
